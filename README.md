# PHP Silex Base

EWU app framework built on the [Silex](http://silex.sensiolabs.org/) PHP micro-framework.

  - HTTP Request/Response Routing
  - Multiple Databases Support
  - Database Migrations
  - Dynamic Form Creation

## Installation
#### Install using [Git](https://git-scm.com/) & [Composer](https://getcomposer.org/).
```
# For more information see git and composer documentation
$ git clone git@source.eastern.ewu.edu:application-design/projects_silex_base.git app
$ cd app
$ composer install
```
#### Apache configuration
Apache should be configured to serve files from the project's `/public` folder. 
Place an updated Directory Directive configuration from below in a `.conf` file 
in the apache conf.d folder.  
```
<Directory "/file/path/to/app/public">
<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /web/context/
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ index.php [QSA,L]
</IfModule>
</Directory>
```
## Configuration
##### Environment Variables
---------
**To prevent sensitive credentials from being archived in code**. Storing
[configuration in the environment](http://www.12factor.net/config) is one of
the tenets of a [twelve-factor app](http://www.12factor.net/). Anything that is
likely to change between deployment environments – such as database credentials
or credentials for 3rd party services – should be extracted from the
code into environment variables.
* DB_USER
* DB_PASSWORD
* DEBUG

##### Configuration File
---------
The platform also supports configuration through a file sitting outside of the 
web path. If a `.env` file exists in the `/config` directory, it's contents will 
be loaded into the application environment. This method should also be used for 
custom development configurations. The  `.env` is included in the `.gitignore` 
file so that it won't be mistakenly checked into a code repository. Below is an 
example
```
# Set variables and values to be loaded into ENV
DEBUG=true
EXAMPLE_DB_DRIVER='pdo_mysql'
EXAMPLE_DB_HOST=127.0.0.1
EXAMPLE_DB_PORT=3306
EXAMPLE_DB_DBNAME=example_db
EXAMPLE_DB_USER=tester
EXAMPLE_DB_PASSWORD=Avsj7XM6%rWC]zRDnH3mqqqZ
```
## Database Management
Database migrations offer the ability to programmatically deploy new versions of
your database schema in a safe, easy and standardized way. Migration files can
be created and deployed using the console application bundled in this framework. 
```
$ # Run this commands from the application root to see if the database is up-to-date.
$ app/console migrations:status
```
```
$ # Run this commands from the application root to apply new migrations to the database.
$ app/console migrations:migrate
```
### Version
0.7

### Composer Packages

The framework depends on an number of PHP projects:

* [silex/silex] - The PHP micro-framework based on the Symfony Components
* [knplabs/console-service-provider] - A console service provider for Silex
* [vlucas/phpdotenv] - Loads environment variables from `.env` to `getenv()`, `$_ENV` and `$_SERVER` automagically.
* [dbtlr/silex-doctrine-migrations] - A Doctrine migration service provider for Silex
* [dflydev/doctrine-orm-service-provider] - Doctrine ORM Service Provider
* [ewu/sso] - Implements EWU SSO through the CAS & SAML protocols
* [twig/twig] - the flexible, fast, and secure template language for PHP
* [symfony/twig-bridge] - Symfony Twig Bridge
* [symfony/form] - Symfony Form Component
* [symfony/validator] - Symfony Validator Component
* [symfony/security-csrf] - Symfony Security Component - CSRF Library
* [symfony/config] - Symfony Config Component
* [symfony/translation] - Symfony Translation Component
