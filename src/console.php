<?php

// Grab the App
$app = require_once __DIR__.'/bootstrap.php';

// Add a console App
$app->register(new Knp\Provider\ConsoleServiceProvider(), array(
    'console.name' => 'Forms Application',
    'console.version' => '1.0.0',
    'console.project_directory' => __DIR__.'/..',
));

// Add Doctrine Migrations to the Console App
$app->register(new \Dbtlr\MigrationProvider\Provider\MigrationServiceProvider(), array(
    'db.migrations.path' => __DIR__.'/../db/migrations',
    'db.migrations.table_name' => 'doctrine_migrations',
));

// return the console App
$console = $app['console'];

return $console;
?>
