# Silex Provided Database Integration

Silex provides integration with the DoctrineServiceProvider for quick and easy
database access.

In order to use the Doctrine Service, an $app->register call needs to be made in config.php
(This file is located under /phpframework/src/config.php)

## Registration

Depending on which database provider you are using, you will need to adjust certain parameters via registration.

These parameters include:

* __driver__ - The database driver to use. Defaults to pdo_mysql.
Valid values: pdo_mysql, pdo_sqlite, pdo_pgsql, pdo_oci, oci8, ibm_db2,     pdo_ibm, pdo_sqlsrv.

* __dbname__ - The name of the database to connect to.

* __host__ - The host of the database to connect to. Defaults to localhost.

* __user__ - The user of the database to connect to. Defaults to root.

* __password__ - The password of the database to connect to.

* __charset__ - Only relevant for pdo_mysql, and pdo_oci/oci8, specifies the charset used when connecting to the database.

* __path__ - Only relevant for pdo_sqlite, specifies the path to the SQLite database.

* __port__ - Only relevant for pdo_mysql, pdo_pgsql, and pdo_oci/oci8, specifies the port of the database to connect to.

### Registration Examples
An example registration for a SQLite db would look like the following. This would register the database 'app.db' which lies in the relative path to the config.

```php
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_sqlite',
        'path'     => __DIR__.'/app.db',
    ),
));
```



The following example will register multiple databases as different names. This registration call will add 2 mySQL databases to the array in dbs.options, allowing for seperate data sources.
Note that in order to facilitate this, 'db.options' was changed to 'dbs.options'.

```php
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'mysql_read' => array(
            'driver'    => 'pdo_mysql',
            'host'      => 'mysql_read.someplace.tld',
            'dbname'    => 'my_database',
            'user'      => 'my_username',
            'password'  => 'my_password',
            'charset'   => 'utf8mb4',
        ),
        'mysql_write' => array(
            'driver'    => 'pdo_mysql',
            'host'      => 'mysql_write.someplace.tld',
            'dbname'    => 'my_database',
            'user'      => 'my_username',
            'password'  => 'my_password',
            'charset'   => 'utf8mb4',
        ),
    ),
));
```

## Usage

Once the database is registered, it is simple to use. Here is an example for how to use a database registered as 'db'.

```php
$app->get('/blog/{id}', function ($id) use ($app) {
    $sql = "SELECT * FROM posts WHERE id = ?";
    $post = $app['db']->fetchAssoc($sql, array((int) $id));

    return  "<h1>{$post['title']}</h1>".
            "<p>{$post['body']}</p>";
});
```

This function call creates a sql query, runs it against the registered database 'db', and returns the results in a formatted header+paragraph.

### Using Multiple Registration Connections

When using multiple database sources, the first registered connection will be recognized as the default.
That is, from the multiple db example above, these two lines would be equivalent:

```php
$app['db']->fetchAll('SELECT * FROM table');

$app['dbs']['mysql_read']->fetchAll('SELECT * FROM table');
```

### More Usage Examples

When using multiple connections, it is best practice to call the full registered namespace, such as

```php
$app['dbs']['mysql_read']
```

A good example of using multiple connections:

```php
$app->get('/blog/{id}', function ($id) use ($app) {
    $sql = "SELECT * FROM posts WHERE id = ?";
    $post = $app['dbs']['mysql_read']->fetchAssoc($sql, array((int) $id));

    $sql = "UPDATE posts SET value = ? WHERE id = ?";
    $app['dbs']['mysql_write']->executeUpdate($sql, array('newValue', (int) $id));

    return  "<h1>{$post['title']}</h1>".
            "<p>{$post['body']}</p>";
});
```

This function creates a selection query for the first database, calls it, then creates an update query for the second database.

Notice the '->executeUpdate()' command, and how it is different than the selection syntax. Each SQL command has a seperate syntax for usage.

## Further information
[DoctrineServiceProvider with Silex](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/)

[Silex Doctrine provider](https://silex.sensiolabs.org/doc/2.0/providers/doctrine.html)

